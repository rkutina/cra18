CRAP18
Crappy RISC Architecture, Plop 18 - INSTRUCTION SET

!WARNING! - THIS THING IS JUST ABOUT AS BETA AS IT GETS

- make this thing just about as ortogonal as reasonably possible, so I could try some OOO stuff later too, just for the heck of it C:

76 5432 1098 7654 3210
00 oooo oooo TTTT SSSS  | does an op 'o' on S and T, also takes in account last value written to T
   0                    | stateless instructions
   
   0000 0000 0000 0000  | NOP alias
   0000 0000            | MOVE  //copies S to T, also to TR
   0000 0001            | =?
   0000 0010            | ADD
   0000 0011            | SUB
   0000 0100            | AND
   0000 0101            | OR/IOR
   0000 0110            | XOR
   0000 0111            | NOT
   0000 1000 0PPP       | POKE
   0000 1000 1PPP       | PUSH
   0000 1001      0PPP  | PEEK
   0000 1001      1PPP  | POP
   0000 1010            | INC
   0000 1011            | DEC
   0000 1100            | SHL   //shift TR left by stuff in src reg
   0000 1101            | SHR   //shift TR right by stuff in src reg
   0000 1110            | 
   0000 1111            | 
   0010 NNNN            | SHLC  //shift TR left by a constant N
   0011 NNNN            | SHRC  //shift TR right by a constant N
   0xxx                 | =?
   
   1                    | 'stateful' and other instructions. may not necessarily match the T S reg positions - ?
    //? whatever, this should be enough already.
           
01 aaaa aaaa aaaa aaaa  | unconditional jump to 'a'
10 ccaa aaaa aaaa aaaa  | conditional jumps
   00                   | jump to 'a' if tr=zero
   01                   | jump to 'a' if last conditional is true. doesn't include cont. checking conditionals
//   10                   | jump to a if cont. check. con. 0 is true
//   11                   | jump to a if cont. check. con. 1 is true
11 DDDD DDDD DDDD DDDD  | shifts TR by 16 left, and ors D into the lowest 16 bits 


REGISTER FILE:
0000: R0, hardwired to zero
0001: R1  \
0010: R2   \
0011: R3    \ General Purpose
0100: R4    / Registers
0101: R5   /
0110: R6  /
0111: R7 /
1000: P0, used by interrupts too
1001: P1  \
1010: P2  /  All sorts of stack pointers
1011: P3 /
1100: //C0 \ used to set continuously checking conditionals
1101: //C1 / 
1110: ?   //options and flags
1111: TR  //mirrored temporary register
